import re
import requests
from bottle import route, run
from bottle import static_file
from bs4 import BeautifulSoup, Comment
from io import BytesIO
from PIL import Image
from pathlib import Path


def get_file_path(url, folder, is_full_path=False):
    dot_split = url.split('.')
    slash_split = dot_split[-2].split('/')
    name = slash_split[-1]
    ext = dot_split[-1]
    full_path = '{}{}.{}'.format(folder, name, ext)
    path = '{}.{}'.format(name, ext)
    if is_full_path:
        return full_path
    return path


def replace_and_insert(html):
    html = html.replace(b'https://habrahabr.ru/', b'http://localhost:8080/')
    regex_word = re.compile(r'\b([Ёёа-яА-Яa-zA-Z]{6})\b')
    soup = BeautifulSoup(html, "html.parser")
    body = soup.find('body')
    for string in body.find_all(string=True):
        if string.parent.name == 'script' or type(string) == Comment:
            continue
        new_string = re.sub(regex_word, r'\1™', string).replace('&plus;', '+')
        string.replace_with(new_string)
    return str(soup)


@route('/<url_path:re:.*>')
def main(url_path):
    url = 'https://habrahabr.ru/{}'.format(url_path)
    r = requests.get(url)
    content = r.content
    if r.headers.get('content-type') == 'text/html; charset=UTF-8':
        return replace_and_insert(content)
    if r.headers.get('content-type') == 'image/png':
        file = get_file_path(url, 'cache/', is_full_path=False)
        file_path = get_file_path(url, 'cache/', is_full_path=True)
        get_file = Path(file_path)
        if get_file.is_file():
            return static_file(file, root='cache')
        else:
            r = requests.get(url)
            i = Image.open(BytesIO(r.content))
            i.save(file_path)
            return static_file(file, root='cache', mimetype='image/png')
    return content


run(host='localhost', port=8080, debug=True)
